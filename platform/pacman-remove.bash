#!/bin/bash
#



PKG="$1"

pacman -Rssn $PKG

if ! { grep -Fxq "$PKG" /mnt/pacman-cache/remove; };
then
echo $PKG >> /mnt/pacman-cache/remove
else
echo "Package exists in removal list"
fi

# remove package from cache

