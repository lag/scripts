#!/bin/bash
# Prepare a hard drive for the Milieudefensie linux desktop image
# 20130709 Maarten te Paske <maarten.te.paske@milieudefensie.nl>
# set -o xtrace

error_exit() {
        echo "error, $1"
        exit 1
}

DRIVE="$1"

ROOT="${DRIVE}1"
SWAP="${DRIVE}2"
MLD="${DRIVE}5"
ROOTSIZE="15000000"

# Check some basic prerequisites
[ $# -ne "1" ]                          && error_exit "usage: $0 <drive>"
[ -x $(which sfdisk) ]                  || error_exit "no sfdisk binary"
[ -x $(which fdisk) ]                   || error_exit "no fdisk binary"
[ -x $(which mkfs.ext4) ]               || error_exit "no mkfs.ext4 binary"
[ -x $(which mkswap) ]                  || error_exit "no mkswap binary"
[ -b "$DRIVE" ]                         || error_exit "drive $DRIVE does not exist"

if sfdisk -l $DRIVE 2>&1 | grep -q "^No partitions found";
then
        echo "Drive $DRIVE does not have a partition table. Will create one now."
        (echo -en "n\np\n1\n\n+20G\nn\np\n2\n\n+8G\nn\ne\n3\n\n\nn\nl\n\n\na\n1\nt\n2\n82\nw\n" | fdisk $DRIVE >/dev/null) || error_exit "could not partition drive $DRIVE, please inspect manually"

        echo "Creating filesystems on newly created partitions"
        mkfs.ext4 -q $ROOT              || error_exit "could not create ext4 filesystem on $ROOT"
        mkswap $SWAP                    || error_exit "could not create swap space on $SWAP"
        mkfs.ext4 -q $MLD               || error_exit "could not create ext4 filesystem on $MLD"
elif ([ -b $ROOT ] && [ -b $SWAP ] && [ -b $MLD ]);
then
        echo "Drive $DRIVE already has the required partitions, checking them one by one."
        [ $(sfdisk -s -u M $ROOT) -ge "$ROOTSIZE" ]     || error_exit "partition $ROOT is too small, please inspect manually"

        read -p "WARNING: partition $ROOT will be erased! Continue [yN]? " DELROOT
        if [ "$DELROOT" = "y" ];
        then
                mkfs.ext4 -q $ROOT      || error_exit "could not create ext4 filesystem on $ROOT"
        fi

        read -p "WARNING: partition $SWAP will be erased! Continue [yN]? " DELSWAP
        if [ "$DELSWAP" = "y" ];
        then
                mkswap $SWAP            || error_exit "could not create swap space on $SWAP"
        fi

        read -p "WARNING: partition $MLD will be erased! Continue [yN]? " DELMLD
        if [ "$DELMLD" = "y" ];
        then
                mkfs.ext4 -q $MLD       || error_exit "could not create ext4 filesystem on $MLD"
        fi
else
        echo "Drive $DRIVE already has a partition table, but it does not seem to suit our layout."
        read -p "WARNING: all data on disk $DRIVE will be lost! Continue [yN]? " DELDRIVE

        [ "$DELDRIVE" = "y" ]           || error_exit "clearly you don't want to continue..."

        echo "Creating new partition table on drive $DRIVE"
        echo -en "n\np\n1\n\n+20G\nn\np\n2\n\n+8G\nn\ne\n3\n\n\nn\nl\n\n\na\n1\nt\n2\n82\nw\n" | fdisk $DRIVE || error_exit "could not partition drive $DRIVE, please inspect manually"

        echo "Creating filesystems on newly created partitions"
        mkfs.ext4 -q $ROOT              || error_exit "could not create ext4 filesystem on $ROOT"
        mkswap $SWAP                    || error_exit "could not create swap space on $SWAP"
        mkfs.ext4 -q $MLD               || error_exit "could not create ext4 filesystem on $MLD"
fi
