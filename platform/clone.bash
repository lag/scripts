#!/bin/bash
# Basic tooling for cloning a Linux desktop image to .tar.gz
# 20130624 Maarten te Paske <maarten.te.paske@milieudefensie.nl>
#          We rely on tar instead of dd, so we do not write zero blocks
# set -o xtrace

error_exit() {
        echo "error, $1"
        exit 1
}

MEDIADIR="/mnt/platform-image"
IMGDIR="$MEDIADIR/images"
SRCDIR="$MEDIADIR/restore"
DRIVE="$1"
IMAGE="$2"
NOW=$(date +%Y%m%d-%H%M%S)
PARTITION="${DRIVE}"

# Check some basic prerequisites
[ $# -ne "2" ]                          && error_exit "usage: $0 <drive> <image> (ie. $0 /dev/sda raring)"
[ -x $(which date) ]                    || error_exit "no date binary"
[ -x $(which tar) ]                     || error_exit "no tar binary"
[ -x $(which gzip) ]                    || error_exit "no gzip binary"
[ -b "$DRIVE" ]                         || error_exit "drive $DRIVE does not exist"
[ -b "$PARTITION" ]                     || error_exit "partition $PARTITION does not exist"
[ -f "$IMGDIR/$IMAGE-$NOW.tar.gz" ]     && error_exit "image file $IMGDIR/$IMAGE-$NOW.tar.gz already exists"

grep -q "$PARTITION" /proc/mounts       && error_exit "partition $PARTITION is already mounted, please unmount first"

mount $PARTITION $SRCDIR                || error_exit "could not mount $PARTITION on $SRCDIR"

echo "Cleaning up some garbage, just in case"
for DIR in tmp home/local; do
        [ -d "$SRCDIR/$DIR" ] && rm -rf "$SRCDIR/$DIR/{.[A-Za-z0-9]*,[A-Za-z0-9]*}"
done

echo "Creating image $IMGDIR/$IMAGE-$NOW.tar.gz from source $SRCDIR ($PARTITION)"
cd $SRCDIR && time tar -v --create --gzip --file  $IMGDIR/$IMAGE-$NOW.tar.gz . || error_exit "could not create $IMGDIR/$IMAGE-$NOW.tar.gz"


if [ -L "$IMGDIR/$IMAGE.tar.gz" ];
then
        read -p "Symlink $IMGDIR/$IMAGE.tar.gz already exists. Do you want to update the link to the new image? Y/n " UPDATELINK
        echo "Updating symbolic link for $IMGDIR/$IMAGE.tar.gz to $IMGDIR/$IMAGE-$NOW.tar.gz"
        [ "$UPDATELINK" = "Y" ] && rm $IMGDIR/$IMAGE.tar.gz && ln -s $IMGDIR/$IMAGE-$NOW.tar.gz $IMGDIR/$IMAGE.tar.gz
else
        echo "Creating symbolic link for $IMGDIR/$IMAGE.tar.gz to $IMGDIR/$IMAGE-$NOW.tar.gz"
        ln -s $IMGDIR/$IMAGE-$NOW.tar.gz $IMGDIR/$IMAGE.tar.gz
fi

cd $OLDPW && umount $PARTITION

