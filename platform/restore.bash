#!/bin/bash
# Basic tooling for restoring a Linux desktop image to hard drive
# 20130624 Maarten te Paske <maarten.te.paske@milieudefensie.nl>
#          We rely on tar instead of dd, so we do not write zero blocks
# set -o xtrace

error_exit() {
        echo "error, $1"
        exit 1
}

MEDIADIR="/mnt/platform-image"
IMGDIR="$MEDIADIR/images"
DSTDIR="/media"
DRIVE="$1"
IMAGE="$2"

PARTITION="${DRIVE}"

# Check some basic prerequisites
[ $# -ne "2" ]                          && error_exit "usage: $0 <drive> <image>"
[ -x $(which fdisk) ]                   || error_exit "no fdisk binary"
[ -x $(which mkfs.ext4) ]               || error_exit "no mkfs.ext4 binary"
[ -x $(which ip) ]                      || error_exit "no ip binary"
[ -x $(which awk) ]                     || error_exit "no awk binary"
[ -x $(which head) ]                    || error_exit "no head binary"
[ -x $(which ssh) ]                     || error_exit "no ssh binary"
[ -b "$DRIVE" ]                         || error_exit "drive $DRIVE does not exist"
[ -b "$PARTITION" ]                     || error_exit "partition $PARTITION does not exist, run prepare-linux first"
[ -f "$IMGDIR/$IMAGE.tar.gz" ]          || error_exit "image $IMAGE does not exist"


read -p "WARNING: partition $PARTITION will be erased! Continue [yN]? " ERASE
[ "$ERASE" = "y" ]                      || error_exit "clearly you don't want to continue..."

echo "Creating a new ext4 filesystem on $PARTITION"
mkfs.ext4 -q $PARTITION                 || error_exit "could not create a new ext4 filesystem on $PARTITION"

grep -q "$DSTDIR" /proc/mounts          && error_exit "mountpoint $DSTDIR already in use"

echo "Mounting newly created filesystem on $DSTDIR"
mount $PARTITION $DSTDIR                || error_exit "could not mount $PARTITION on $DSTDIR"
[ -d "$DSTDIR/dev" ]                    || mkdir -p $DSTDIR/dev

echo "Restoring image contents to $DSTDIR"
cd $DSTDIR
tar --extract --gunzip --file $IMGDIR/$IMAGE.tar.gz

