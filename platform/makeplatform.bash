#!/bin/bash
# Prepare and place LAG linux desktop image
# 20140327 dreamer @ laglab.org
# set -o xtrace

error_exit() {
        echo -e " \e[31mError: \e[39m $1"
        exit 1
}

DRIVE="$1"
IMAGE="$2"

BOOT="${DRIVE}1"
ROOT="${DRIVE}2"
SWAP="${DRIVE}3"

IMGDIR="/mnt/platform-image"
RTDIR="/media/"
BTDIR="${RTDIR}/boot/"

# Check some basic prerequisites
[ $# -ne "2" ]                          && error_exit "usage: $0 <drive> <image>"
[ -x $(which dd) ]	                || error_exit "no dd binary"
[ -x $(which sfdisk) ]                  || error_exit "no sfdisk binary"
[ -x $(which fdisk) ]                   || error_exit "no fdisk binary"
[ -x $(which mkfs.ext4) ]               || error_exit "no mkfs.ext4 binary"
[ -x $(which mkswap) ]                  || error_exit "no mkswap binary"
[ -b "$DRIVE" ]                         || error_exit "drive $DRIVE does not exist"
[ -f "$IMGDIR/$IMAGE-boot.tar.gz" ]     || error_exit "image $IMAGE-boot does not exist"
[ -f "$IMGDIR/$IMAGE-root.tar.gz" ]     || error_exit "image $IMAGE-root does not exist"


read -p "WARNING: disk $DRIVE will be erased! Continue [yN]? " ERASE
[ "$ERASE" = "y" ]                      || error_exit "clearly you don't want to continue..."

# Make sure partition table is destroyed
dd if=/dev/zero of=$DRIVE bs=512 count=1

# Make 3 partitions: 300M /boot, 30G / and 4G swap
(echo -en "n\np\n1\n\n+300M\nn\np\n2\n\n+30G\nn\np\n3\n\n+4G\n\nw" | fdisk $DRIVE >/dev/null) || error_exit "could not partition drive $DRIVE, please inspect manually"

echo -e " \e[32m* \e[39mCreating filesystems on newly created partitions"
mkfs.ext4 -q $BOOT	              	|| error_exit "could not create ext4 filesystem on $BOOT"
mkfs.ext4 -q $ROOT      	        || error_exit "could not create ext4 filesystem on $ROOT"
mkswap $SWAP                    	|| error_exit "could not create swap space on $SWAP"

sleep 2

echo -e " \e[32m* \e[39mRe-reading partition-table"
sfdisk -R $DRIVE			|| error_exit "could not re-read partition table of $DRIVE"

grep -q "$RTDIR" /proc/mounts 	        && error_exit "mountpoint $RTDIR already in use"

echo -e " \e[32m* \e[39mMounting newly created filesystem on $RTDIR"
mount $ROOT $RTDIR	                || error_exit "could not mount $PARTITION on $RTDIR"

[ -d "$RTDIR/dev" ]			|| mkdir -p $RTDIR/dev

echo -e " \e[32m* \e[39mRestoring root-image contents to $RTDIR"
cd $RTDIR
tar --extract --gunzip --file $IMGDIR/$IMAGE-root.tar.gz

echo -e " \e[32m* \e[39mMounting newly created filesystem on $BTDIR"
mount $BOOT $BTDIR                      || error_exit "could not mount $PARTITION on $RTDIR"

echo -e " \e[32m* \e[39mRestoring boot-image contents to $BTDIR"
cd $BTDIR
tar --extract --gunzip --file $IMGDIR/$IMAGE-boot.tar.gz

echo -e " \e[32m* \e[39mConfiguring hostname and finishing install"
read -p "What hostname do you want for this system? # " HNAME

echo -e " \e[32m* \e[39mPreparing chroot"
for i in {proc,sys,dev}; do
	mount -o bind /$i $RTDIR/$i;
done

chroot $RTDIR /bin/bash -c "echo $HNAME > /etc/hostname; hostname -F /etc/hostname"	|| error_exit "setting name $HNAME failed"
chroot $RTDIR grub-mkconfig -o /boot/grub/grub.cfg	|| error_exit "update-grub failed"
chroot $RTDIR mkinitcpio -p linux  			|| error_exit "update-initramfs failed"
chroot $RTDIR grub-install $DRIVE			|| error_exit "grub-install failed on $DRIVE"

umount -Rl $RTDIR

echo -e " \e[32m* \e[39mFinished! you can now reboot to start your new system called $HNAME"
exit
